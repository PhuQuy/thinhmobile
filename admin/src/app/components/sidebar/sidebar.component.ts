import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Trang chủ',  icon: 'dashboard', class: '' },
    { path: 'user-profile', title: 'Thông tin cá nhân',  icon:'person', class: '' },
    { path: 'product-list', title: 'Quản lý sản phẩm',  icon:'content_paste', class: '' },
    { path: 'blogs', title: 'Quản lý tin tức',  icon:'content_paste', class: '' },
    { path: 'typography', title: 'Quản lý khung chat',  icon:'library_books', class: '' },
    { path: 'icons', title: 'Quản lý danh mục',  icon:'bubble_chart', class: '' },
    { path: 'maps', title: 'Bản đồ',  icon:'location_on', class: '' },
    { path: 'notifications', title: 'Thông báo',  icon:'notifications', class: '' },
    // { path: 'upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
