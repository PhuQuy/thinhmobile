import { ChangeDetectionStrategy, Component, Output, Input } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent {
  @Input() config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 6,
    currentPage: 1
  };

  pageChange(page) {
    this.config.currentPage = page;
    // if(this.isTop) {
    // } else {
    //   $('body,html').animate({
    //     scrollTop: 140
    //   }, 500, 'swing');
    // }
  }

  show(page: any) {
    if(page.getLastPage() == 1) {
      return false;
    }

    return true;
  }
}
