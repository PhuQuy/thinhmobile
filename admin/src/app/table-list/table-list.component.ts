import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UploadService } from './../services/upload.service';
import { ProductService } from './../services/product.service';
import { CategoriesService } from './../services/category.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Product } from './../models/product';
import { cloudinary } from '../../environments/environment';

declare var jQuery: any;
@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css'],
  providers: [ProductService, UploadService, AngularFireAuth, CategoriesService]
})
export class TableListComponent implements OnInit {
  public items = [];
  public loading = false;
  public product: Product = new Product();
  public error: any;
  public photos: any;
  public uploading = false;
  public logoURL = '../assets/img/no-image.jpg';

  public options: any = {
    inlineMode: false,
    placeholderText: 'Chỉnh sửa mô tả ở đây!',
    charCounterCount: false,
    // Set the image upload URL.
    imageUploadURL: cloudinary.imageUploadURL,
    // Additional upload params.
    imageUploadParams: {
      upload_preset: cloudinary.upload_preset,
      api_key: cloudinary.api_key
    },

    events: {
      'froalaEditor.image.uploaded': function(e, editor, response) {
        var _parseJSON = jQuery.parseJSON;
        response = _parseJSON(response);
        editor.image.remove();
        editor.image.insert(response.secure_url);
      }
    }
  }

  @ViewChild('logoButton') logoButton: ElementRef;
  @ViewChild('nextButton') nextButton: ElementRef;

  categories: any;

  constructor(private afAuth: AngularFireAuth, private productService: ProductService, private upSvc: UploadService, protected categoriesService: CategoriesService) {
    categoriesService.getCategories().subscribe(categories => {
      this.categories = categories;
    })
  }

  createNewProduct() {
    // this.afAuth.authState.subscribe(auth => {
    //   if (auth) {
    //     this.loading = true;
    //     this.product.author = auth.displayName;
    //     this.items.map(item => {
    //       this.product.tags.push(item.value);
    //     })
    //     this.productService.addProduct(this.product).then(value => {
    //       this.product = new Product();
    //       this.loading = false;
    //       this.items = [];
    //       this.photos = '';
    //     }).catch(error => {
    //       this.error = error;
    //       this.loading = false;
    //     });
    //   }
    // });

    this.loading = true;
    this.items.map(item => {
      this.product.tags.push(item.value);
    })
    this.product.slug = this.getSlug(this.product.title);

    if(this.product.newPrice) {
      this.product.sale = ((this.product.price - this.product.newPrice) / this.product.price) * 100;
    }

    this.productService.addProduct(this.product).then(value => {
      this.product = new Product();
      this.loading = false;
      this.items = [];
      this.photos = '';
    }).catch(error => {
      this.error = error;
      this.loading = false;
    });
  }

  getSlug(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str.replace(/\s+/g, '-');
  }

  selectCategory(category) {
    this.product.category = category;
  }

  selectProductType(type) {
    this.product.prodType = type;
  }

  changeLogo() {
    this.logoButton.nativeElement.click();
  }

  detectPhoto(event) {
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.uploading = true;
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          console.log(uploaded);
          this.uploading = false;
          this.product.photoUrl = uploaded;
          this.logoURL = uploaded;
        })
      }
    }
  }

  detectPhotos(event) {
    if (event.target.files) {
      this.product.photos = new Array();
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          this.product.photos.push(uploaded);

        })
      }
    }
  }

  ngOnInit() {
  }

}
