import { Injectable } from '@angular/core';
import { Product } from './../models/product';
import { Iproduct } from './../models/iproduct';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class ProductService {

  private basePath = '/products';

  private itemsCollection: AngularFirestoreCollection<Iproduct>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<Iproduct>(this.basePath);
  }

  getProductsList(query?) {
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Iproduct;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getSpecial() {
    this.itemsCollection = this.angularFirestore.collection('products', ref => ref.where('special', '==', true));
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Iproduct;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getMobile() {
    this.itemsCollection = this.angularFirestore.collection('products', ref => ref.where('prodType', '==', 'mobile'));
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Iproduct;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getTablet() {
    this.itemsCollection = this.angularFirestore.collection('products', ref => ref.where('prodType', '==', 'tablet'));
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Iproduct;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getOnSale() {
    this.itemsCollection = this.angularFirestore.collection('products', ref => ref.where('sale', '>', 0));
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Iproduct;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  // Return a single observable Product
  getProduct(key: string) {
    const itemPath = `${this.basePath}/${key}`;
    return this.angularFirestore.doc<Product>(itemPath).valueChanges();
  }

  // Create a bramd new product
  createProduct(product: Product) {
    var data = this.mapProduct(product);
    const timestamp = this.timestamp;
    return this.itemsCollection.add({ ...data, createdAt: timestamp });
  }

  mapProduct(product: Product) {
    return {
      title: product.title,
      slug: product.slug,
      description: product.description,
      star: product.star,
      author: product.author,
      kg: product.kg,
      box: product.box,
      photoUrl: product.photoUrl,
      price: product.price,
      sale: product.sale,
      special: product.special,
      photos: product.photos,
      instock: product.instock,
      fullDescription: product.fullDescription,
      newPrice: product.newPrice,
      provider: product.provider,
      prodType: product.prodType,
      category: product.category,
      tags: product.tags,
    }
  }

  addProduct(product: Product) {
    // return this.db.list(`${this.basePath}/`).push(product);
    var data = this.mapProduct(product);
    const timestamp = this.timestamp;
    return this.itemsCollection.add({ ...data, createdAt: timestamp });
  }


  // Update an exisiting product
  updateProduct(key: string, value: any): void {
    // this.productsRef.update(key, value)
  }

  // Deletes a single product
  deleteProduct(key: string): void {
    // this.productsRef.remove(key)
  }

  // Deletes the entire list of products
  deleteAll(): void {
    // this.productsRef.remove()
  }
}
