import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

@Component({
  selector: 'app-typography',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.css'],
  providers: [AngularFireDatabase]
})
export class TypographyComponent implements OnInit {
  protected path = 'chats';
  public chats: any;
  public messages: any;
  public message: any;
  public chatRef: any;

  constructor(protected db: AngularFireDatabase) {
    // this.db.list(this.path).valueChanges().subscribe(chats => {
    //   if (chats) {
    //     this.chats = chats;
    //   }
    // });

    this.chats = this.db.list(this.path).snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.val()
        const $key = a.payload.key;
        return { $key, ...data };
      });
    });
  }

  chatWith(key) {
    const path = 'chats/' + key;
    this.chatRef = this.db.list(path);
    this.chatRef.valueChanges().subscribe(messages => {
      if (messages) {
        this.messages = messages;
      }
    })
  }

  send() {
    this.chatRef.push({ message: this.message, admin: true });
    this.message = '';
  }

  ngOnInit() {
  }

}
