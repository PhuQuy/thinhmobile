import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../services/category.service';
import { PaginationInstance } from 'ngx-pagination';
import { UploadService } from './../services/upload.service';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.css'],
  providers: [CategoriesService, UploadService]
})
export class IconsComponent implements OnInit {
  categories: any;
  category_name: string;
  category_parent: any;
  category_type: string;

  uploading = false;
  url: string = '';

  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(public categoriesService: CategoriesService, private upSvc: UploadService) {
    categoriesService.getCategories().subscribe(categories => {
      this.categories = categories;
    });
  }

  ngOnInit() {
  }

  selectCategory(value) {
    this.category_parent = value;
  }

  selectProductType(value) {
    this.category_type = value;
  }

  changeProductType(value, category) {
    category.type = value;
    this.categoriesService.updateCategory(category);
  }

  createCategory() {
    if (this.category_parent) {
      var data2 = {
        name: this.category_name,
        parent: {
          id: this.category_parent
        },
        type: this.category_type,
        slug: this.getSlug(this.category_name)
      }
      this.categoriesService.createCategory(data2).then(respone => {
        this.category_name = '';
      });
    } else {
      var data = {
        name: this.category_name,
        parent: null,
        type: this.category_type,
        slug: this.getSlug(this.category_name)
      }
      this.categoriesService.createCategory(data).then(respone => {
        this.category_name = '';
      });
    }
  }

  detectPhoto(event, category) {
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.uploading = true;
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          this.uploading = false;
          category.image = uploaded;
          console.log(category);
          this.categoriesService.updateCategory(category);
        })
      }
    }
  }

  getSlug(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str.replace(/\s+/g, '-');
  }

  deteleteCategory(id) {
    this.categoriesService.deteleteCategory(id);
  }
}
