export class Product {
  $key: string;
  id: string;
  title:string = '';
  slug: string = '';
  description:string = '';
  star: number = 0;
  author: string = null;
  kg: boolean = false;
  box: boolean = true;
  sale: number = 0;
  special: boolean = false;
  photoUrl: string = null;
  photos: string[] = [];
  price: number = null;
  instock: boolean = true;
  fullDescription: string = '';
  newPrice: number = null;
  category: string = null;
  provider: string = '';
  prodType: string = '';
  tags: string[] = [];

  constructor() {
  }
}
