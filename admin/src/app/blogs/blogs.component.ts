import { Component, OnInit } from '@angular/core';
import { Blog } from './../models/blog';
import { UploadService } from './../services/upload.service';
import { BlogService } from './../services/blog.service';
import { cloudinary } from '../../environments/environment';

declare var jQuery: any;
@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.scss'],
  providers: [UploadService, BlogService]
})
export class BlogsComponent implements OnInit {
  blog: Blog = new Blog();
  public items = [];
  public loading = false;
  public error: any;
  public uploading = false;

  public options: any = {
    inlineMode: false,
    placeholderText: 'Chỉnh sửa mô tả ở đây!',
    charCounterCount: false,
    // Set the image upload URL.
    imageUploadURL: cloudinary.imageUploadURL,
    // Additional upload params.
    imageUploadParams: {
      upload_preset: cloudinary.upload_preset,
      api_key: cloudinary.api_key
    },

    events: {
      'froalaEditor.image.uploaded': function(e, editor, response) {
        var _parseJSON = jQuery.parseJSON;
        response = _parseJSON(response);
        editor.image.remove();
        editor.image.insert(response.secure_url);
      }
    }
  }

  constructor(private blogService: BlogService, private upSvc: UploadService) { }
  createNewBlog() {
    this.blog.slug = this.getSlug(this.blog.title);
    this.blog.star =4;
    console.log(this.blog);
    this.blogService.addBlog(this.blog).then(value => {
      this.blog = new Blog();
      this.loading = false;
      this.items = [];
      // this.photos = '';
    }).catch(error => {
      this.error = error;
      this.loading = false;
    });
  }
  ngOnInit() {
  }

  getSlug(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str.replace(/\s+/g, '-');
  }
  detectPhoto(event) {
    if (event.target.files) {
      let files = event.target.files;
      for (var i = 0; i < files.length; i++) {
        this.uploading = true;
        this.upSvc.pushUpload(files.item(i)).subscribe(uploaded => {
          this.uploading = false;
          this.blog.photoUrl = uploaded;
        })
      }
    }
  }
}
