'use strict';


function getData(req) {
  var params = req.originalUrl.split('/');
  var doc = params[2];
  // return new Promise(function(resolve, reject) {
  //   var product = admin.firestore().collection('products').where('slug', '==', doc).onSnapshot(snapshot => {
  //       snapshot.forEach(function(doc) {
  //         console.log(doc.data())
  //         resolve(doc.data());
  //       });
  //     })
  //     .catch(err => {
  //       resolve(null);
  //       console.log('Error getting documents', err);
  //     });
  //
  //     // product();
  // });


}

module.exports.detectBot = function (userAgent) {
  // List of bots to target, add more if you'd like

  const bots = [
    // crawler bots
    'googlebot',
    'bingbot',
    'yandexbot',
    'duckduckbot',
    'slurp',
    // link bots
    'twitterbot',
    'facebookexternalhit',
    'linkedinbot',
    'embedly',
    'baiduspider',
    'pinterest',
    'slackbot',
    'vkShare',
    'facebot',
    'outbrain',
    'W3C_Validator'
  ]

  const agent = userAgent.toLowerCase()

  for (const bot of bots) {
    if (agent.indexOf(bot) > -1) {
      return true;
    }
  }

  return false;

}
