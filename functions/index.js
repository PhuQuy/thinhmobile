const functions = require('firebase-functions');
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
const express = require('express');
const exphbs = require('express-handlebars');
const app = express();
const botDetect = require('./botDetect.js');
const appUrl = 'thinh-ahihi.firebaseapp.com';

app.engine('handlebars', exphbs({
  defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

app.get('*', (req, res) => {
  const isBot = botDetect.detectBot(req.headers['user-agent']);
  var url = req.protocol + '://' + appUrl + req.originalUrl;
  if (isBot) {

    var params = req.originalUrl.split('/');
    var doc = params[2];

    admin.firestore().collection('products').where('slug', '==', doc).onSnapshot(snapshot => {
          snapshot.forEach(function(doc) {
            var data = doc.data();
            res.render('index', {
              title: data.title,
              description: data.description,
              url: url,
              image: data.photoUrl
            });
          });
        })
  } else {
    res.render('index');
  }

});

exports.app = functions.https.onRequest(app);
