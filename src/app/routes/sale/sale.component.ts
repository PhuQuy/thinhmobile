import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './../../services/categories/categories.service';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from './../../services/product/product.service';

declare var $: any;
@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css'],
  providers: [CategoriesService, ProductService]
})
export class SaleComponent {

  categories: any;
  category: any = {
    name: ''
  };
  key: string = 'title';
  products: any =[];
  filter: string = '';
  constructor(protected categoriesService: CategoriesService, private router: ActivatedRoute, private productService: ProductService) {
    $('body,html').animate({
      scrollTop: 1
    }, 800, 'swing');

    productService.getOnSale().subscribe(products => this.products = products);
  }
}
