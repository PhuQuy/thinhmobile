import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { BlogService } from './../../../services/blog/blog.service';

declare var $: any;
@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  providers: [BlogService]
})
export class BlogDetailsComponent implements OnInit {
  imageURL: string;
  blog: any;
  constructor(private router: ActivatedRoute, private blogService: BlogService) {
    this.router.params.subscribe(params => {
      $('body,html').animate({
        scrollTop: 1
      }, 800, 'swing');
      blogService.getBySlug(params['key']).subscribe(blogs => {
        if (!blogs[0]) {
          return;
        }
        this.blog = blogs[0];
        this.imageURL = this.blog.photoUrl;
      });

    });
  }

  ngOnInit() {
  }

}
