import { Component, OnInit } from '@angular/core';
import { SharedService } from './../../services/shared/shared.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html'
})
export class BlogsComponent implements OnInit {
  protected default = true;

  constructor(protected sharedService: SharedService) {
    this.sharedService.setDefault(this.default);
  }

  ngOnInit() {
  }

}
