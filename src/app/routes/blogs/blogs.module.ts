import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { RecentBlogsModule } from './../../components/recent-blogs/recent-blogs.module';

import { BlogsComponent } from './blogs.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailsComponent } from './blog-details/blog-details.component';

const routes: Routes = [
  {
    path: '', component: BlogsComponent,
    children: [
        { path: '', component: BlogListComponent },
        { path: ':key', component: BlogDetailsComponent }
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RecentBlogsModule
  ],
  declarations: [BlogsComponent, BlogListComponent, BlogDetailsComponent]
})
export class BlogsModule { }
