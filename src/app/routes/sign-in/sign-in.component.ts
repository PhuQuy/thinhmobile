import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase} from 'angularfire2/database';
import { Router } from "@angular/router";
import { User } from './../../models/user';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  providers: [AngularFireAuth, AngularFireDatabase]
})
export class SignInComponent implements OnInit {

  error: any;
  email: any;
  password: any;
  public showSpinner = false;
  authState: any = null;
user: User = new User();

  constructor(public afAuth: AngularFireAuth, public router: Router, protected db: AngularFireDatabase) { }

  ngOnInit() {
  }

  signupByEmailAndPassword(formData) {
    this.showSpinner = true;
    console.log(this.user.email)
  return this.afAuth.auth.createUserWithEmailAndPassword(this.user.email, this.password)
    .then((user) => {
      this.showSpinner = false;
      this.authState = user;
      this.updateUserData();
      user.sendEmailVerification().then(function() {
        // Email sent.
      }).catch(function(error) {
        // An error happened.
        this.showSpinner = false;
      });
    })
    .catch(error => {
      this.error = error;
      this.showSpinner = false;
    });
    }

    get currentUserId(): string {
        return this.authenticated ? this.authState.uid : '';
      }

      get authenticated(): boolean {
        return this.authState !== null;
      }
    private updateUserData(): void {
    // Writes user name and email to realtime db
    // useful if your app displays information about users or for admin features

    const path = `users/${this.currentUserId}`; // Endpoint on firebase
    const userRef: any = this.db.object(path);
    userRef.update(this.user)
      .catch(error => console.log(error));
  }

}
