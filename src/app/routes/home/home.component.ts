import { Component, OnInit } from '@angular/core';
import { ProductService } from './../../services/product/product.service';
import { CategoriesService } from './../../services/categories/categories.service';

declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ProductService, CategoriesService]
})
export class HomeComponent implements OnInit {

  public imageURL = "https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/03/27/104367476-galaxy-s8-s8-plus.jpg?v=1490702859";
  public image2 = "/assets/images/demo.jpg";

  public imagesURL = ["/assets/images/demo.jpg",
    'https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/03/27/104367476-galaxy-s8-s8-plus.jpg?v=1490702859',
    'http://i1-news.softpedia-static.com/images/news2/samsung-galaxy-s8-to-pack-infinity-display-iris-scanner-no-logo-on-the-front-512179-2.jpg',
    'http://techrum.vn/chevereto/images/2017/05/30/3FC08.jpg',
    'https://cdn.vox-cdn.com/uploads/chorus_asset/file/8245279/akrales_170327_1549_A_0140.0.jpg',
    'http://static4.businessinsider.com/image/58dabff42dfbdb7c008b4c4a-1200/3-you-can-charge-the-galaxy-s8-with-a-wireless-charging-pad-theres-also-fast-charging-which-charges-the-s8-faster-than-normal.jpg'
  ]

  public products: any;
  public showSpinner= true;
  public tablets: any = [];
  public others: any;
  hotCategories: any;
  hotTablets: any;

  constructor(private productService: ProductService, private categoryService: CategoriesService) {
    // this.products = this.productService.getProducts('mobile');
    // this.tablets = this.productService.getProducts('tablet');
    this.others = this.productService.getProductsList();
    this.hotCategories = this.categoryService.getNewest('mobile');
    this.hotTablets = this.categoryService.getNewest('tablet');
    $('body,html').animate({
      scrollTop: 1
    }, 800, 'swing');
  }


  ngOnInit() {
    this.hotCategories.subscribe(products => {
      this.showSpinner = false;
      console.log(products)
    });
  }

}
