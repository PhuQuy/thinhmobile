import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { CircularTabComponent } from './../../components/circular-tab/circular-tab.component';
import { SliderComponent } from './../../components/slider/slider.component';
import { RecentBlogsModule } from './../../components/recent-blogs/recent-blogs.module';
import { ProductItemModule } from './../../components/product-item/product-item.module';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    RecentBlogsModule,
    ProductItemModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomeComponent, CircularTabComponent, SliderComponent]
})
export class HomeModule { }
