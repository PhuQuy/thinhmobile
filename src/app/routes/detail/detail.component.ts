import { Component, OnInit } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from './../../services/product/product.service';
import { SharedService } from './../../services/shared/shared.service';

declare var $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  providers: [ProductService]
})
export class DetailComponent implements OnInit {

  public index = -1;
  public imageURL: string = "";

  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
  public product: any;
  public sameProducts: any;

  constructor(private router: ActivatedRoute, private productService: ProductService, protected sharedService: SharedService) {

    this.router.params.subscribe(params => {
      $('body,html').animate({
        scrollTop: 1
      }, 800, 'swing');
      productService.getBySlug(params['key']).subscribe(products => {
        if (!products[0]) {
          return;
        }
        this.product = products[0];
        this.imageURL = this.product.photoUrl;
        productService.getByCategory(this.product.category).subscribe(products => {
          this.sameProducts = products;
        })
      });

    });
  }

  showModal(prod) {
    this.sharedService.emitProductChange(prod);
  }

  ngOnInit() {
    this.carouselTileItems = [
      { "small": "/assets/images/lot-chuot-logitech-4.jpg", "big": "/assets/images/lot-chuot-logitech-4.jpg" },
      { "small": "assets/images/lot-chuot-logitech-5.jpg", "big": "assets/images/lot-chuot-logitech-5.jpg" },
      { "small": "assets/images/lot-chuot-logitech-6.jpg", "big": "assets/images/lot-chuot-logitech-6.jpg" },
    ];


    this.carouselTile = {
      grid: { xs: 3, sm: 2, md: 2, lg: 4, all: 0 },
      slide: 2,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: false
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }

}
