import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgxCarouselModule } from 'ngx-carousel';
import { DetailComponent } from './detail.component';
import { ProductItemModule } from './../../components/product-item/product-item.module';

const routes: Routes = [
  {
    path: ':key', component: DetailComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgxCarouselModule,
    ProductItemModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailComponent]
})
export class DetailModule { }
