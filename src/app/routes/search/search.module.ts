import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { Ng2OrderModule } from 'ng2-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ProductItemModule } from './../../components/product-item/product-item.module';

const routes: Routes = [
  {
    path: ':key', component: SearchComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    Ng2OrderModule,
    FormsModule,
    Ng2SearchPipeModule,
    ProductItemModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
