import { Component, ViewChildren, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CategoriesService } from './../../services/categories/categories.service';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from './../../services/product/product.service';

declare var $: any;
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [CategoriesService, ProductService]
})
export class SearchComponent implements AfterViewInit {
  @ViewChildren('filteredItems') filteredItems;
  categories: any;
  category: any = {
    name: ''
  };
  key: string = 'title';
  products: any =[];
  filter: string = '';
  constructor(protected categoriesService: CategoriesService, private router: ActivatedRoute, private productService: ProductService, private cdr: ChangeDetectorRef) {
    $('body,html').animate({
      scrollTop: 1
    }, 800, 'swing');

    this.router.params.subscribe(params => {
      this.filter = params['key'];
      productService.getProductsList().subscribe(products => this.products = products);
    });
  }

  ngAfterViewInit() {
    this.filteredItems = [];
    this.cdr.detectChanges();
  }
}
