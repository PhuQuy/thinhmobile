import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AngularFireAuth]
})
export class LoginComponent implements OnInit {
  error: any;
  email: any;
  password: any;
  public showSpinner = false;

  constructor(public router: Router,public afAuth: AngularFireAuth) { }

  ngOnInit() {
  }

  loginByEmailAndPassword(formData) {
      this.showSpinner = true;
      this.afAuth.auth.signInWithEmailAndPassword(formData.value.email, formData.value.password).then(
        (success) => {
          this.error = null;
          this.showSpinner = false;
          this.router.navigate(['/']);
        }).catch(
        (err) => {
          this.showSpinner = false;
          this.error = err.message;
        });
    }
}
