import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list.component';
import { ProductItemModule } from './../../components/product-item/product-item.module';

const routes: Routes = [
  {
    path: ':key', component: ListComponent,
  }
];
@NgModule({
  imports: [
    CommonModule,
    ProductItemModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListComponent]
})
export class ListModule { }
