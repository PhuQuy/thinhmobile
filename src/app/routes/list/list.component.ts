import { Component, OnInit } from '@angular/core';
import { CategoriesService} from './../../services/categories/categories.service';
import { ActivatedRoute } from "@angular/router";
import { ProductService } from './../../services/product/product.service';

declare var $: any;
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [CategoriesService, ProductService]
})
export class ListComponent implements OnInit {
  categories: any;
  category: any = {
    name: ''
  };
  products: any;
  constructor(protected categoriesService: CategoriesService, private router: ActivatedRoute, private productService: ProductService) {
    $('body,html').animate({
      scrollTop: 1
    }, 800, 'swing');
    this.router.params.subscribe(params => {
      categoriesService.getBySlug(params['key']).subscribe(category => {
        if(!category[0]) {
          return;
        }
        this.category = category[0];
        this.categoriesService.getRootCategories(category[0]).subscribe(categories => {
          this.categories = categories;
        })
      })
      this.products = productService.getByCategory(params['key']);
    });

  }

  ngOnInit() {
  }

}
