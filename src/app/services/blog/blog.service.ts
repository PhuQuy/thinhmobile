import { Injectable } from '@angular/core';
import { Blog } from './../../models/blog';
import { IBlog } from './../../models/iblog';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class BlogService {
  basePath = 'blogs';
  items: any;

  // blog: IBlog;

  private itemsCollection: AngularFirestoreCollection<IBlog>;
  private itemDoc: AngularFirestoreDocument<IBlog>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<IBlog>(this.basePath);

    // this.items = this.itemsCollection.snapshotChanges().map(changes => {
    //   return changes.map(a => {
    //     const data = a.payload.doc.data() as IBlog;
    //     data.id = a.payload.doc.id;
    //     return data;
    //   });
    // });

  }

  getBlogs(query?) {
    // return this.itemsRef.snapshotChanges().map(arr => {
    //   return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    // })

    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as IBlog;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  search(start) {
    // return this.db.list('blogs', ref => ref.limitToFirst(4).orderByChild('title').startAt(start)).snapshotChanges().map(arr => {
    //   return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    // })

   return this.angularFirestore.collection(`${this.basePath}/`, ref => ref.limit(4).orderBy('title').startAt(start)).valueChanges();
  }

  mapItem(blog: Blog) {
    return {
      title: blog.title,
      description: blog.description,
      slug: blog.slug,
      star: blog.star,
      author: blog.author,
      authorImage: blog.authorImage,
      photoUrl: blog.photoUrl,
      content: blog.content,
      tags: blog.tags

    }
  }


  getBySlug(slug: string) {
    // return this.db.list(`${this.basePath}/`, ref => ref.orderByChild('slug').equalTo(slug)).snapshotChanges().map(arr => {
    //   return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    // })
    this.itemsCollection = this.angularFirestore.collection(`${this.basePath}/`, ref => ref.where('slug', '==', slug))
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as IBlog;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  // Return a single observable IBlog
  get(key: string): Observable<any> {
    const itemPath = `${this.basePath}/${key}`;
    // return this.db.object(itemPath).valueChanges();
    return this.angularFirestore.doc<IBlog>(itemPath).valueChanges();
  }

  deleteBlog(blog: Blog) {
    // return this.db.list(`${this.basePath}/`).remove(blog.$key);
  }

  addBlog(blog: Blog) {
    console.log(blog);
    var data = this.mapItem(blog);
    const timestamp = this.timestamp;
    return this.itemsCollection.add({
      ...data, createdAt: timestamp
    });
    // return this.db.list(`${this.basePath}/`).push(blog);
  }

  addItem(IBlog: IBlog) {

    this.itemsCollection.add(IBlog);
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }
}
