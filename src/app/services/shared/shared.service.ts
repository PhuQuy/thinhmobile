import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SharedService {
  heading: string;
  key: string;

  public color_root = "white";
  public background_root = "#262324";
  public border_color_root = "white";
  public default_root = false;

  protected color = "white";
  protected background = "#fec909";
  protected border_color = "#fec909";
  protected default = true;
  
  getColorRoot () {
    return this.color_root;
  }

  getBorderColorRoot() {
    return this.border_color_root;
  }

  getBackgroundRoot() {
    return this.background_root;
  }
  setColor(color) {
    this.color = color;
  }

  getColor() {
    return this.color;
  }

  setBackground(background) {
    this.background = background;
  }

  getBackground() {
    return this.background;
  }

  setBorderColor(border_color) {
    this.border_color = border_color;
  }

  getBorderColor() {
    return this.border_color;
  }

  setDefault(isDefault) {
    this.default = isDefault;
  }

  getDefault() {
    return this.default;
  }

  setHeading(newValue) {
    this.heading = newValue; //you can also do validation or other things here
  }
  getHeading() {
    return this.heading;
  }

  getKey() {
    return this.key;
  }

  setKey(newValue) {
    this.key = newValue;
  }

  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  getEmitChangeSource() {
    return this.emitChangeSource;
  }

  private emitProductSource = new Subject<any>();
  // Observable string streams
  emitProductSource$ = this.emitProductSource.asObservable();

  emitProductChange(change: any) {
    this.emitProductSource.next(change);
  }

  getEmitProductChangeSource() {
    return this.emitProductSource;
  }

  private emitSearch = new Subject<any>();
  // Observable string streams
  emitSearch$ = this.emitSearch.asObservable();

  emitSearchChange(change: any) {
    this.emitSearch.next(change);
  }

}
