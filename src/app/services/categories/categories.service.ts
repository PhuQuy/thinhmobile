import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observer } from "rxjs/Observer";
import { Observable } from "rxjs/Observable";

@Injectable()
export class CategoriesService {

  private basePath = '/categories';

  private itemsCollection: AngularFirestoreCollection<any>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<any>(this.basePath);
  }

  getCategories(query?) {
    return this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        if (data.parent) {
          this.getCategory(data.parent.id).then(name => {
            data.parent.name = name;
          });
        }
        return data;
      });
    });
  }

  //   updateSlug() {
  //     const th= this;
  //     this.angularFirestore.collection<any>(this.basePath).ref
  //       .onSnapshot(function(querySnapshot) {
  //         querySnapshot.forEach(function(doc) {
  //           doc.ref.set({ slug: th.getSlug(doc.data().name) })
  //         });
  //       });
  //   }
  //
  //   getSlug(alias) {
  //     var str = alias;
  //     str = str.toLowerCase();
  //     str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
  //     str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
  //     str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
  //     str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
  //     str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
  //     str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
  //     str = str.replace(/đ/g,"d");
  //     str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
  //     str = str.replace(/ + /g," ");
  //     str = str.trim();
  //     return str.replace(/\s+/g, '-');
  // }

  getRootCategories(category) {
    return this.angularFirestore.collection<any>(this.basePath, ref => ref.where('parent.id', '==', category.id)).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        this.getChild(data.id).subscribe(categories => {
          data.categories = categories;
        });
        return data;
      });
    });
  }

  getRootAllCategories() {
    return this.angularFirestore.collection<any>(this.basePath, ref => ref.where('parent', '==', null)).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        this.getChild(data.id).subscribe(categories => {
          data.categories = categories;
        });
        return data;
      });
    });
  }

  childs: any = [];

  getBySlug(slug) {
    this.childs = [];
    return this.angularFirestore.collection<any>(this.basePath, ref => ref.where('slug', '==', slug)).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        if (data.parent) {
          this.getRootCategory(data.parent.id).then(parent => {
            data.para = this.childs;
          });
        }
        return data;
      });
    });
  }

  createCategory(category: any) {
    return this.itemsCollection.add({ ...category });
  }

  getChild(id) {
    return this.angularFirestore.collection(this.basePath, ref => ref.where('parent.id', '==', id)).valueChanges();
  }

  getNewestPhone() {
    return this.angularFirestore.collection<any>(this.basePath, ref => ref.where('image', '>', '')).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getNewest(type) {
    return this.angularFirestore.collection<any>(this.basePath, ref => ref.where('type', '==', type).where('image', '>', '')).snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getRootCategory(id) {
    const categoryService = this;
    return new Promise((resolve, reject) => {
      this.itemsCollection.doc(id).ref.get().then(function(doc) {
        if (doc.exists) {
          categoryService.childs.unshift(doc.data());
          if (doc.data().parent) {
            resolve(doc.data());
            categoryService.getRootCategory(doc.data().parent.id).then(parent => {
              doc.data().parent = parent;
            });
          } else {
            resolve(doc.data());
          }
        } else {
          resolve(null);
        }

      }).catch(function(error) {
        console.log("Error getting document:", error);
      });
    });
  }

  getCategory(id) {
    return new Promise((resolve, reject) => {
      this.itemsCollection.doc(id).ref.get().then(function(doc) {
        if (doc.exists) {
          resolve(doc.data().name);
        } else {
          resolve('');
        }

      }).catch(function(error) {
        console.log("Error getting document:", error);
      });
    });
  }

  deteleteCategory(id) {
    this.itemsCollection.doc(id).delete().then(function() {
      console.log("Document successfully deleted!");
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });
  }
}
