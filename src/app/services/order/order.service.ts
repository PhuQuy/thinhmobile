import { Injectable } from '@angular/core';
import { Order } from './../../models/order';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()
export class OrderService {

  private basePath = '/orders';

  private itemsCollection: AngularFirestoreCollection<any>;

  constructor(private angularFirestore: AngularFirestore) {
    this.itemsCollection = angularFirestore.collection<any>(this.basePath);
  }

  // Create a bramd new product
  createOrder(order: Order) {
    order.createdAt = this.timestamp;
    return this.itemsCollection.add({ ...order });
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

}
