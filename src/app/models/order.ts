import { Product } from './product';

export class Order {
  id: string;
  userID: string = '';
  name: string;
  phone: string;
  email: string = '';
  ward: string;
  address: string;
  cart: Product;
  description: string = '';
  createdAt: any;

  constructor() {
  }
}
