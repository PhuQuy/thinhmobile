import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';

import { SharedService } from './services/shared/shared.service';

import { NavComponent } from './components/nav/nav.component';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { PopupComponent } from './components/popup/popup.component';
import { BookingComponent } from './components/booking/booking.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: './routes/home/home.module#HomeModule'
  },
  {
    path: 'chi-tiet',
    loadChildren: './routes/detail/detail.module#DetailModule'
  },
  {
    path: 'tin-tuc',
    loadChildren: './routes/blogs/blogs.module#BlogsModule'
  },
  {
    path: 'danh-muc',
    loadChildren: './routes/list/list.module#ListModule'
  },
  {
    path: 'dang-nhap',
    loadChildren: './routes/login/login.module#LoginModule'
  },
  {
    path: 'dang-ky',
    loadChildren: './routes/sign-in/sign-in.module#SignInModule'
  },
  {
    path: 'tim-kiem',
    loadChildren: './routes/search/search.module#SearchModule'
  },
  {
    path: 'giam-gia',
    loadChildren: './routes/sale/sale.module#SaleModule'
  }
];

export const firebaseConfig = {
  apiKey: "AIzaSyBdUbpz1bSOkovxt6JMp1Fljb9VfiEXQT0",
  authDomain: "thinh-ahihi.firebaseapp.com",
  databaseURL: "https://thinh-ahihi.firebaseio.com",
  projectId: "thinh-ahihi",
  storageBucket: "thinh-ahihi.appspot.com",
  messagingSenderId: "295115692225"
};

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    PopupComponent,
    BookingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [SharedService],
  bootstrap: [AppComponent],
  exports: [AppComponent]
})
export class AppModule { }
