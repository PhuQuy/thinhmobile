import { Component } from '@angular/core';
import { SharedService } from './services/shared/shared.service';
import { CategoriesService } from './services/categories/categories.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CategoriesService]
})
export class AppComponent {

  public defaultBackGround = "#fec909";
  public is_root = false;
  product: any;
  constructor(protected sharedService: SharedService, private router: Router, private CategoriesService: CategoriesService) {
    this.router.events.subscribe((event) => {
      if(router.url === '/') {
        this.is_root = true;
      } else {
        this.is_root = false;
      }
    });

    sharedService.emitProductSource$.subscribe(
      product => {
        this.product = product;
        console.log(product);
      });
  }

  color() {
    return this.sharedService.getColor();
  }

  background() {
    return this.sharedService.getBackground();
  }

  border_color() {
    return this.sharedService.getBorderColor();
  }

  is_default() {
    return this.sharedService.getDefault();
  }

  colorRoot() {
    return this.sharedService.getColorRoot();
  }

  background_root() {
    return this.sharedService.getBackgroundRoot();
  }

  border_color_root() {
    return this.sharedService.getBorderColorRoot();
  }
}
