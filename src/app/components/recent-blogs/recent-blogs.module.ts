import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RecentBlogsComponent } from './recent-blogs.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [RecentBlogsComponent],
  exports: [RecentBlogsComponent]
})
export class RecentBlogsModule { }
