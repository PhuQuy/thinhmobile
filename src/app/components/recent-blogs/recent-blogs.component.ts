import { Component, OnInit } from '@angular/core';
import { BlogService } from './../../services/blog/blog.service';

@Component({
  selector: 'app-recent-blogs',
  templateUrl: './recent-blogs.component.html',
  styleUrls: ['./recent-blogs.component.css'],
  providers: [BlogService]
})
export class RecentBlogsComponent implements OnInit {

  blogs: any;
  constructor(protected blogService: BlogService) {
    this.blogs = blogService.getBlogs();
  }

  ngOnInit() {
  }

}
