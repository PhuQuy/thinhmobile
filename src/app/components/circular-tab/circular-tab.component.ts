import { Component, OnInit } from '@angular/core';

declare var $: any;
@Component({
  selector: 'app-circular-tab',
  templateUrl: './circular-tab.component.html'
})
export class CircularTabComponent implements OnInit {
  index = 0;
  constructor() {
  }

  prevousUpDate(index) {
    this.index = index;
  }

  ngOnInit() {

  }

}
