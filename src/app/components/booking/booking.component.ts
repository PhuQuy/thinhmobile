import { Component, OnInit } from '@angular/core';
import { SharedService } from './../../services/shared/shared.service';
import { Order } from './../../models/order';
import { OrderService } from './../../services/order/order.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
  providers: [OrderService]
})
export class BookingComponent implements OnInit {
  product: any;
  order: Order = new Order();
  loading = true;

  constructor(protected sharedService: SharedService, protected orderService: OrderService) {
    sharedService.emitProductSource$.subscribe(
      product => {
        this.product = product;
        this.loading = true;
      });
  }

  ngOnInit() {
  }

  booking() {
    this.order.cart = this.product;
    this.orderService.createOrder(this.order).then(response => {
      this.loading = false;
    });
  }

}
