import { Component, Input } from '@angular/core';
import { Inject, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import { CategoriesService } from './../../services/categories/categories.service';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";

import { User } from './../../models/user';

declare var $: any;
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers: [AngularFireAuth]
})
export class NavComponent {

  @Input() default: boolean = false;
  @Input() color: string = '#717171';
  @Input() background_color: string = '#fec909';
  @Input() border_color: string = '#fec909';

  public fixed: boolean = false;
  public showDrop: boolean = false;
  public showMenuMobile: string = 'closeNav';

  categories: any;
  user: any;
  destroyMan: any;
  constructor( @Inject(DOCUMENT) private document: Document, protected angularFireAuth: AngularFireAuth, private router: Router, protected categoriesService: CategoriesService) {
    this.destroyMan = this.angularFireAuth.authState.subscribe(auth => {
      if (auth) {
        this.user = auth;
        console.log(auth);
      }
    });
    categoriesService.getRootAllCategories().subscribe(categories => {
      this.categories = categories;
      console.log(categories);
    })
  }

  logout() {
    this.angularFireAuth.auth.signOut();
    location.reload();
  }

  ngOnDestroy() {

  }

  search(value) {
    this.router.navigate(['/tim-kiem', value]);
  }

  showMenuBar() {
    this.showMenuMobile = 'openNav';
  }

  closeMenuBar() {
    this.showMenuMobile = 'closeNav';
  }
  ahhi() {
    console.log('aaaaa')
  }
  routeLink(link) {
    this.closeMenuBar();
    this.router.navigate([link]);
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    if (number > 100) {
      this.fixed = true;
    } else if (this.fixed && number < 100) {
      this.fixed = false;
    }
  }
}
