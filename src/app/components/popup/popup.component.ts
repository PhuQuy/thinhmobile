import { Component, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

declare var $: any;
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  providers: [AngularFireDatabase, AngularFireAuth]
})
export class PopupComponent {
  chat_list: string[] = [];
  public show = false;
  public message: string = '';
  itemsRef: any;
  public uid: string = '';
  messages: any;
  chat: any;
  auth: any;
  public minimize = false;

  @ViewChild('msgFocus') msgFocus: any;

  constructor(protected db: AngularFireDatabase, public afAuth: AngularFireAuth) {
    // const path = 'chats';
    // this.itemsRef = this.db.list(path);
    this.afAuth.authState.subscribe((auth) => {
      if (!auth) {
        return;
      }
      this.auth = auth;
      this.uid = auth.uid;
      const path = 'chats/' + this.uid;
      this.itemsRef = this.db.list(path);
      this.itemsRef.valueChanges().subscribe(messages => {
        if (messages) {
          this.messages = messages;
        }
        this.srollBottom();
      })
    });
  }

  srollBottom() {
    $("#message_content").animate({ scrollTop: $('#message_content').prop("scrollHeight") }, 800, 'swing');
  }

  showChat() {
    this.show = true;
    if (!this.auth) {
      this.afAuth.auth.signInAnonymously();
    }
    this.srollBottom();
    setTimeout(() => { this.msgFocus.nativeElement.focus();this.srollBottom();});
  }

  send() {
    this.srollBottom();
    if (this.message === '' || this.message == 'indefined' || this.message.length === 0) {
      return;
    }
    this.itemsRef.push({ message: this.message, admin: false });
    this.chat_list.push(this.message);
    this.message = '';
  }

  ngOnInit() {
  }
}
